package th.ac.kku.srisuriyanukul.siravit.currentgeocoder;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    RadioGroup radioGroup;
    RadioButton use_latlng, use_address;
    EditText lat_et, lng_et, address_et;
    Geocoder geocoder;
    Button fetch;
    TextView result_tv;
    String result = "";

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioGroup = findViewById(R.id.radiogroup);
        use_latlng = findViewById(R.id.latlng_rb);
        use_address = findViewById(R.id.address_rb);
        lat_et = findViewById(R.id.lat_et);
        lng_et = findViewById(R.id.lng_et);
        address_et = findViewById(R.id.address_et);
        fetch = findViewById(R.id.fetch_btn);
        result_tv = findViewById(R.id.result_tv);

        final RxPermissions rxPermissions = new RxPermissions(this); // where this is an Activity or Fragment instance
        rxPermissions
                .request(Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                .subscribe(granted -> {
                    if (granted) { // Always true pre-M
                        // I can control the camera now
                    } else {
                        // Oups permission denied
                    }
                });
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        use_latlng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lat_et.setEnabled(true);
                lng_et.setEnabled(true);
                address_et.setEnabled(false);
                lat_et.requestFocus();
            }
        });

        use_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lat_et.setEnabled(false);
                lng_et.setEnabled(false);
                address_et.setEnabled(true);
                address_et.requestFocus();
            }
        });

        final Geocoder geocoder = new Geocoder(this, Locale.getDefault());

        fetch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String result;
                if (use_latlng.isChecked()) {
                    try {
                        List<Address> list = geocoder.getFromLocation(
                                Double.parseDouble(lat_et.getText().toString()),
                                Double.parseDouble(lng_et.getText().toString()), 1);
                        if (list != null && list.size() > 0) {
                            Address address = list.get(0);
                            result = address.getAddressLine(0) + ", " + address.getLocality();
                            result_tv.setText(result);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        List<Address> list = geocoder.getFromLocationName(
                                address_et.getText().toString(), 1);
                        if (list != null && list.size() > 0) {
                            Address address = list.get(0);
                            result = "Lat: " + address.getLatitude() + "\n" + "Lng: " + address.getLongitude();
                            result_tv.setText(result);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Toast.makeText(this, "onLocationChanged" , Toast.LENGTH_LONG).show();
        lat_et.setText("" + location.getLatitude());
        lng_et.setText("" + location.getLongitude());
    }
}
