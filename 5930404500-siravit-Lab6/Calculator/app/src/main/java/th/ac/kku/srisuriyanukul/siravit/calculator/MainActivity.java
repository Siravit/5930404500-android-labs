package th.ac.kku.srisuriyanukul.siravit.calculator;

import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity  extends AppCompatActivity { //implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

//    EditText et1,et2;
//    TextView tvResult, tv_SW;
//    Button btn_cal;
//    RadioGroup rg_op;
//    RadioButton rb_add, rb_min, rb_mul, rb_div;
//    Switch sw;
//
//    float val1, val2;
//    float result = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome);

//        initInstance();
//
//        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int width = size.x;
//        int height = size.y;
//
//        Toast toast = Toast.makeText(getApplicationContext(),"Width = " + width + ", Height = " + height, Toast.LENGTH_LONG);
//        toast.show();
//
//        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if(isChecked){
//                    tv_SW.setText("เปิด");
//                }
//                else {
//                    tv_SW.setText("ปิด");
//                }
//            }
//        });
    }

//    private void initInstance() {
//        et1 = findViewById(R.id.et_firstNO);
//        et2 = findViewById(R.id.et_secondNO);
//        tvResult = findViewById(R.id.tv_result);
//        btn_cal = findViewById(R.id.btn_calculate);
//        rg_op = findViewById(R.id.rg_op);
//        rb_add = findViewById(R.id.rbAdd);
//        rb_min = findViewById(R.id.rbMinus);
//        rb_mul = findViewById(R.id.rbMulti);
//        rb_div = findViewById(R.id.rbDiv);
//        sw = findViewById(R.id.sw);
//        tv_SW = findViewById(R.id.tv_sw);
//
//        btn_cal.setOnClickListener(this);
//        rg_op.setOnCheckedChangeListener(this);
//
//    }

//    public void acceptNumber() {
//        try {
//            val1 = Float.parseFloat(et1.getText().toString());
//            val2 = Float.parseFloat(et2.getText().toString());
//        }
//        catch(NumberFormatException e) {
//            showToast("Please enter only a number");
//        }
//    }

//    public void showToast(String msg){
//        Toast toast = Toast.makeText(getApplicationContext(),msg, Toast.LENGTH_LONG);
//        toast.setGravity(Gravity.CENTER, 0, 0);
//        toast.show();
//    }

//    private void calculate(int id){
//        long start = System.currentTimeMillis();
//        acceptNumber();
//        switch (id) {
//            case R.id.rbAdd:
//                result = val1 + val2;
//                break;
//            case R.id.rbMinus:
//                result = val1 - val2;
//                break;
//            case R.id.rbMulti:
//                result = val1 * val2;
//                break;
//            case R.id.rbDiv:
//                if (val2 == 0) {
//                    showToast("Please divide by a non-zero number");
//                }
//                else {
//                    result = val1 / val2;
//                }
//                break;
//        }
//        tvResult.setText(" = " + result);
//
//        long runTime = System.currentTimeMillis() - start;
//        Log.d("Calculation", "computation time = " + Float.toString((((float)runTime))/1000.0f ));
//
//    }

//    public void onClick(View v) {
//        if (v == btn_cal) {
//            calculate(rg_op.getCheckedRadioButtonId());
//        }
//    }

//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.app_menu,menu);
//        return true;
//    }

//    public  boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == R.id.action_setting) {
//            Toast.makeText(MainActivity.this,
//                    "Choose action settings",Toast.LENGTH_LONG).show();
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//    @Override
//    public void onCheckedChanged(RadioGroup group, int checkedId) {
//        calculate(checkedId);
//    }

}


