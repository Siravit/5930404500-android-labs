package th.ac.kku.srisuriyanukul.siravit.wdpviewdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.lang.reflect.Modifier;

public class MainActivity extends AppCompatActivity {

    EditText et1, et2;
    TextView tv_result;
    Button button;
    Float result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et1 = findViewById(R.id.et_firstNO);
        et2 = findViewById(R.id.et_secondNO);
        tv_result = findViewById(R.id.tv_result);
        button = findViewById(R.id.add_btn);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Float val1 = Float.parseFloat(et1.getText().toString());
                Float val2 = Float.parseFloat(et2.getText().toString());
                result = val1 + val2;
                tv_result.setText(" = " + result.toString());

            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("op1", et1.getText().toString());
        outState.putString("op2", et2.getText().toString());
        outState.putString("result", tv_result.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        et1.setText(savedInstanceState.getString("op1"));
        et2.setText(savedInstanceState.getString("op2"));
        tv_result.setText(savedInstanceState.getString("result"));
    }
}
