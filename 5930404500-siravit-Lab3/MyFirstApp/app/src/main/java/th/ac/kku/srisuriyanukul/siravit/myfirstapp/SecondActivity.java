package th.ac.kku.srisuriyanukul.siravit.myfirstapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second2);

        tv = findViewById(R.id.textView);
        tv.setText(getIntent().getStringExtra("NAME") + " " + getString(R.string.text_plus) + " " + getIntent().getStringExtra("PHONE"));
    }
}
