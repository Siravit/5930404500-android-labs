package th.ac.kku.srisuriyanukul.siravit.nationapicaller;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    ProgressBar progressBar;
    TextView responseView;
    String urlVal;
    EditText urlTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        responseView = (TextView) findViewById(R.id.responseView);
        urlTxt = (EditText) findViewById(R.id.urlText);
        Button queryBt = (Button) findViewById(R.id.queryButton);
        queryBt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new RetrieveFeedTask().execute();
            }
        });
    }

    class RetrieveFeedTask extends AsyncTask<Void, Void, String> {

        private Exception exception;

        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            responseView.setText("");
        }

        protected String doInBackground(Void... urls) {
            try {
                urlVal = urlTxt.getText().toString();
                Log.i("INFO", "url = " + urlVal);
                URL urlAddr = new URL(urlVal);
                HttpURLConnection urlConnection = (HttpURLConnection) urlAddr.openConnection();
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    return stringBuilder.toString();
                } finally{
                    urlConnection.disconnect();
                }
            }
            catch(Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        protected void onPostExecute(String response) {
            if(response == null) {
                response = "THERE WAS AN ERROR";
            }
            progressBar.setVisibility(View.GONE);
            Log.i("INFO", " response = " + response);
            try {
                if (urlVal.equals("https://fb.kku.ac.th/krunapon/json/iptest.json")) {
                    JSONObject object = (JSONObject) new JSONTokener(response).nextValue();
                    String ip = object.getString("ip");
                    responseView.setText(ip);
                } else if (urlVal.equals("https://fb.kku.ac.th/krunapon/json/nations.json")) {
                    for (int i = 0; i < urlVal.length(); i++) {
                        JSONObject object = (JSONObject) new JSONTokener(response).nextValue();
                        JSONArray nations = object.getJSONArray("nations");
                        JSONObject nation = (JSONObject) nations.get(i);
                        String name = (String) nation.get("name");
                        String location = (String) nation.get("location");
                        responseView.append(name + " is in " + location + "\n");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}