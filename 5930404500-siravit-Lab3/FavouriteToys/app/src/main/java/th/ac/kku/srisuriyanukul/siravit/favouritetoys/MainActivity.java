package th.ac.kku.srisuriyanukul.siravit.favouritetoys;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView greenTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setContentView(R.layout.content_main);

        greenTV = findViewById(R.id.green);
        greenTV.setText("GREEN");
        greenTV.setGravity(Gravity.CENTER);
        greenTV.setTextSize(30);
    }
}