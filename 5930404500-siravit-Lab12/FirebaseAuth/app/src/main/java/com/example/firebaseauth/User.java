package com.example.firebaseauth;

import java.util.HashMap;
import java.util.Map;

public class User {
    public String username;
    public String email;

    public User() {

    }

    public User(String username, String email) {
        this.username = username;
        this.email = email;
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("username",username);
        result.put("email",email);
        return result;
    }
}
//
//        ((Button)findViewById(R.id.bt_update)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String sUsername = usernameEdittext.getText().toString();
//                String sEmail = emailEdittext.getText().toString();
//                FirebaseDatabase db = FirebaseDatabase.getInstance();
//                DatabaseReference dbRef = db.getReference("/message/users/");
//                Map<String, Object> childUpdates = new HashMap<>();
//
//                childUpdates.put(sUsername + "/email/", sEmail);
//
//                dbRef.updateChildren(childUpdates);
//
//            }
//        });