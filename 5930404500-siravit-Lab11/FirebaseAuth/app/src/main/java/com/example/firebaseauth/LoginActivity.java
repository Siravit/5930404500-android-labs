package com.example.firebaseauth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    TextView dis_tv;
    Button logout_btn;
    FirebaseAuth mAuth;
    DatabaseReference dbRef;
    FirebaseDatabase firebaseDatabase;
    final String TAG = "siravit";

    EditText updateUser, updateEmail;
    EditText addUser1, addEmail1, addUser2, addEmail2;
    Button update_btn, add_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        firebaseDatabase = FirebaseDatabase.getInstance();
        dbRef = firebaseDatabase.getReference("message");
//        writeDB();
//        readDB();
        writeNewUser("siravit","Siravit","big.siravit@gmail.com");
        readNewUser();

        updateUser = findViewById(R.id.updateUser_et);
        updateEmail = findViewById(R.id.updateEmail_et);
        update_btn = findViewById(R.id.update_btn);

        update_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbRef.child("users").child(updateUser.getText().toString()).child("email").setValue(updateEmail.getText().toString());
            }
        });

        addUser1 = findViewById(R.id.addUser1_et);
        addUser2 = findViewById(R.id.addUser2_et);
        addEmail1 = findViewById(R.id.addEmail1_et);
        addEmail2 = findViewById(R.id.addEmail2_et);
        add_btn = findViewById(R.id.add_btn);

        add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbRef.child("users").child(addUser1.getText().toString()).child("email").setValue(addEmail1.getText().toString());
                dbRef.child("users").child(addUser1.getText().toString()).child("username").setValue(addUser1.getText().toString());
                dbRef.child("users").child(addUser2.getText().toString()).child("email").setValue(addEmail2.getText().toString());
                dbRef.child("users").child(addUser2.getText().toString()).child("username").setValue(addUser2.getText().toString());
            }
        });

        dis_tv = findViewById(R.id.dis_tv);
        logout_btn = findViewById(R.id.logout_btn);

        mAuth = FirebaseAuth.getInstance();

        String userEmail= getIntent().getExtras().getString("Email");
        dis_tv.setText(userEmail + " is logged in successfully");
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser user = mAuth.getCurrentUser();
        //updateUI(currentUser);
        if (user != null) {
            logout_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mAuth.signOut();
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            });
        }
    }

    private void writeDB() {
        dbRef.setValue("Hello,World");
    }

    private void readDB() {
        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                Log.d(TAG,"Value is: " + value);
                Toast.makeText(LoginActivity.this, "Value is " + value,Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w(TAG, "Failed to read value ", databaseError.toException());
            }
        });
    }

    private void writeNewUser(String userId, String name, String email){
        String key = dbRef.child("users").push().getKey();
        User user = new User(name, email);
        Map<String, Object> userValues = user.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/users/" + userId, userValues);
        dbRef.updateChildren(childUpdates);
    }

    private void readNewUser() {
        dbRef.child("users").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                User user = dataSnapshot.getValue(User.class);
                Toast.makeText(LoginActivity.this,"Username is " + user.toMap().get("username"),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                User user = dataSnapshot.getValue(User.class);
                Toast.makeText(LoginActivity.this,"Email is " + user.toMap().get("email"),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
