package th.ac.kku.srisuriyanukul.siravit.myfirstapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText editText = findViewById(R.id.et_name);
        final EditText editText4 = findViewById(R.id.et_phone);
        Button button = findViewById(R.id.bt_submit);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String name = editText.getText().toString();
                String phone = editText4.getText().toString();
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                intent.putExtra("NAME",name);
                intent.putExtra("PHONE",phone);
                startActivity(intent);
            }
        });
    }
}
