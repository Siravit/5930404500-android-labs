package th.ac.kku.srisuriyanukul.siravit.explicitintent;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CODE = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void okButtonClicked(View view) {
        EditText name = (EditText) findViewById (R.id.editText);
        // To do 1. create new intent
        Intent i = new Intent(this,ResultActivity.class);
        // To do 2. put extra value with the intent
        i.putExtra("name" , name.getText().toString());
        // To do 3. use startActivityForResult with REQUEST_CODE
        startActivityForResult(i,REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE) {
            if (data.hasExtra("returnKey1")){
                // Todo 1. get data from ResultActivity
                Toast.makeText(this,data.getExtras().getString("returnKey1"),Toast.LENGTH_SHORT).show();
                // Todo 2. set the value of TextView with the received data
                TextView result = findViewById(R.id.result);
                result.setText(data.getExtras().getString("returnKey1"));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
