package th.ac.kku.srisuriyanukul.siravit.geocoder;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    EditText lat_et,lng_et,address_et;
    RadioButton use_latlng,use_address;
    Button fetch;
    TextView result_tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lat_et = findViewById(R.id.lat_et);
        lng_et = findViewById(R.id.lng_et);
        address_et = findViewById(R.id.address_et);

        use_latlng = findViewById(R.id.latlng_rb);
        use_address = findViewById(R.id.address_rb);

        fetch = findViewById(R.id.fetch_btn);

        result_tv = findViewById(R.id.result_tv);

        use_latlng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lat_et.setEnabled(true);
                lng_et.setEnabled(true);
                address_et.setEnabled(false);
                lat_et.requestFocus();
            }
        });

        use_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lat_et.setEnabled(false);
                lng_et.setEnabled(false);
                address_et.setEnabled(true);
                address_et.requestFocus();
            }
        });

        final Geocoder geocoder = new Geocoder(this, Locale.getDefault());

        fetch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String result;
                if (use_latlng.isChecked()) {
                    try {
                        List<Address> list = geocoder.getFromLocation(
                                Double.parseDouble(lat_et.getText().toString()),
                                Double.parseDouble(lng_et.getText().toString()), 1);
                        if (list != null && list.size() > 0) {
                            Address address = list.get(0);
                            result = address.getAddressLine(0) + ", " + address.getLocality();
                            result_tv.setText(result);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        List<Address> list = geocoder.getFromLocationName(
                                address_et.getText().toString(), 1);
                        if (list != null && list.size() > 0) {
                            Address address = list.get(0);
                            result = "Lat: " + address.getLatitude() + "\n" + "Lng: " + address.getLongitude();
                            result_tv.setText(result);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
