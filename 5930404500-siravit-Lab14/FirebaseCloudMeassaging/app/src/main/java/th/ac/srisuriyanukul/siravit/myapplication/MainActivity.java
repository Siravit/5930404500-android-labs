package th.ac.srisuriyanukul.siravit.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button crash_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        crash_btn = findViewById(R.id.crash_btn);
        crash_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forceCrash(v);
            }
        });
    }

    public void forceCrash(View v){
        throw new RuntimeException("This is a crash");
    }
}

